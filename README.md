# DEC: the Dumb Easy Configuration language

***Note, this may all change at any point - be afraid***

## Ideals

DEC is a specification for a *user-to-machine* markup and configuration language, which values the following, in order:

1. Simplicity
2. Readability
3. Ease of Use
4. Ease of Parsing

DEC accomplishes these goals by giving more power and expressiveness and making less assumptions about data and reporting it truthfully to the programmer. Put simply: it tells you what's in the file and nothing else.

## Quick Start

A given piece of DEC content is expressed as a list of `key: value` pairs, where `key` is a string and a `value` is a string, list of strings or list of key-value pairs.

### Mapping a Key to a String

Let's look at a simple example of matching a key to a value:

```
My Message: Hello World
```

Here, there's only one key-value pair, where the key is `My Message` and the value is the single string `Hello World`. The key and value are both strings of UTF-8 characters.

```
My Message: Hey Buddy:: I'm talking to you
```

Of course, in the odd example in which you'd like to express `::`, you could escape each of those with `::::`. And so on, and so forth.

### Using Comments

Comments can be added using the `#` character, which allows for meta-information to be stored alongside other DEC content. A comment includes all characters after the `#` until the end of the line.

```
My Message: Hello World # This is my message
```

The text `This is my message` isn't parsed. Like before, you could include a literal `#` character in a key or value using `##`, as with the `:` character.

```
## of Configuration Languages: infinite
```

### Mapping Keys to Strings

I can write multiple key-value pairs in the same file, such as the following:

```
My Name: John Davis
My Job: Exterminator
```

This is true if the value is a list, key-value pair or otherwise.

### Mapping Keys to Lists of Strings

Here's an example of using lists of strings:

```
Things I Like:
    Ice Cream
    Root Beer
    Computers

Things I Dislike:
    Cactuses
    Paper Cuts
    Stack Overflow Errors
```

A value is known to be a list when the colon is followed by a newline. From that point on, all values that immediately follow the key and are one layer further indented (the exact definitions of this will be returned to) than the key are part of the list.
It is invalid to write a list whose element begins directly after the colon, such as:

```
Things I Like: Ice Cream
    Root Beer
    Computers
```

### Mapping a Key to a Key-Value Pair

The value of a key-value pair cannot be a single key, although it can be a single string. For example, the following is not considered proper DEC content:

```
Michael: Age: 21
John: Age: 25
```

As you can see, it's not very useful to map an individual key another key-value pair, as you've just essentially made an extra-long key. The above would have two keys, named `Michael` and `John` with values of `Age: 21` and `Age: 25` respectively.

### Mapping a Keys to a List of Key-Value Pairs

Key-value pairs as values becomes more useful when used in lists.

```
Michael:
    Age: 21
    Occupation: Plumber
    OS: Plan 9
John:
    Age: 32
    Occupation: Popcorn Salesman
    OS: Linux
```

As you can see, you can use key-value pairs connected to key-vale pairs to represent basic OOP ideas in text form.

### Advanced Example

Here's an advanced example of using DEC to convey information:

```
# Some places that exist
Locations:
    White House:
        House Number: 1600
        Street: Pennsylvania Avenue
        Inhabitants: 
            The President
            The First Lady
    Google HQ:
        House Number: 1600
        Street: Amphitheater Parkway
        Inhabitants: 
            Rob Pike
            Sundar Pichai

# Some kinds of food I enjoy
Cuisines:
    French:
        From: France
        Notable: French Onion Soup
    Chinese:
        From: China
        Notable: Dumplings
    Thai:
        From: Thailand
        Notable: Pad Thai
```

In the above example, there are two immediate keys, `Locations` and `Cuisines`. Each of those keys' values are key-value pairs themselves, and so on and so forth. This is relatively complex data, but is easy to parse with just a cursory glance.

## Formal Specification

### What Does 'Formal Specification' Mean?

This section is intended not as a guide to DEC syntax, but as a guide to edge-cases and specifications which aren't needed in normal usage.

There are four major definitions needed for the DEC language, comments, key-value pairs, whitespace and lists.

### Comments

Comments are pieces of text not intended to be actually parsed, but are meant for readers of the DEC content to read only. They are any text which follows a `#` character. Two consecutive `#` characters do not create a comment but instead represent a `#` character as data.

Example:

```
This: is data
# This is not
```

### Key-Value pairs

A key-value pair is made of two parts, a key and a value. A key is a string which can contain any series of Unicode characters (with the exception of the `:` character, which would be expressed as `::`). A value is one of the following datatypes:

1. A string
    - This is a literal, UTF-8 encoded string
    - This is the base datatype in DEC, as there are no numbers, times, booleans or whatever else you'd like to throw in
    - This is also a single-line string, as there is no multi-line string functions in this DEC specification
2. A list of strings
    - This would be expressed as an array of strings
    - This should be used minimally and only when data is meant to be unorganized
    - A list always starts one indent-layer further than the key on a new line
3. A list of key-value pairs
    - This would be expressed as a hash-map or equivalent structure
    - A list always starts one indent-layer further than the key on a new line

```
First Key: has a string after it
Second Key: # Data here is invalid
    Has not one
    not two
    but three strings in it
Third Key:
    Sub Key ##1: Has a string in it
    Sub Key ##2:
        Has a list
        Of strings
```

### Whitespace

Whitespace is defined as any grouping of tabs or space characters. The key to a well-spaced document, as any programmer will tell you, is not the kind of whitespace, but the consistency of whitespace. If you like tabs, use tabs. If you like spaces, use spaces. If you find a document is already written in \$BAD-WHITESPACE you prefer \$GOOD-WHITESPACE, just switch your editor's settings or use a `sed 's/$BAD-WHITESPACE/$GOOD-WHITESPACE'`. It's not terribly hard.

With that in mind, DEC, when counting whitespace before a line, assumes each tab to be equal to 4 spaces. This is not a judgement agains using tabs as whitespace, just a convenience. Right now, [the argument is pretty much split based on language](https://ukupat.github.io/tabs-or-spaces/). 4 as a figure itself is entierly subjective, but is used because it places a pretty good gap between two lines on different indent levels. THIS IS NOT SAYING YOU HAVE TO USE THAT. DEC uses difference in number of spaces as its exclusive metric for whitespace measuring. 

# Implementations

* Golang - [DEC](https://gitlab.com/rvs314/decgo)
* Lua - [DEC](https://gitlab.com/rvs314/declua)
